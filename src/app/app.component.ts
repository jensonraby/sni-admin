import { CoreService } from './shared/services/core/core.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private location: LocationStrategy, private coreService: CoreService, private router: Router) {
    this.location.onPopState(() => {
      console.log('route detected');
      // history.pushState(null, null, this.router.url);
      // set isBackButtonClicked to true.
      // this.coreService.setBackClicked(true);
      // return false;
    });
  }
  title = 'sni-admin';
}


