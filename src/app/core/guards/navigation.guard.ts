import { Injectable } from '@angular/core';

import { CanDeactivate, CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { CoreService } from 'src/app/shared/services/core/core.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationGuard implements CanActivate {
  constructor(private router: Router) { }
  // canDeactivate() {
  //   console.log(345666)
  //   // will prevent user from going back
  //   // if (this.someNavigationService.getBackClicked()) {
  //     // this.someNavigationService.setBackClicked(false);
  //     // push current state again to prevent further attempts.
  //     history.pushState(null, null, location.href);
  //     return false;
  //   // }
  //   // return true;
  // }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!localStorage.getItem('sniUser') || localStorage.getItem('sniUser') == 'false') {
      this.router.navigate(['user/login']);
      return false
    }
    return true;
  }
}