import { NavigationGuard } from './core/guards/navigation.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLayoutComponent } from './shared/components/app-layout/app-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: "dashboard",
    pathMatch: 'full'
  },
  {
    path: 'user',
    loadChildren: './module/user-module/user.module#UserModule'
  },
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [NavigationGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: './module/dashboard/dashboard.module#DashboardModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
