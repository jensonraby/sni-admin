import { SideMenuMobileComponent } from './shared/components/side-menu-mobile/side-menu-mobile.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { SideMenuComponent } from './shared/components/side-menu/side-menu.component';
import { AppLayoutComponent } from './shared/components/app-layout/app-layout.component';
import { HttpConfigInterceptor } from './core/interceptor/httpconfig.interceptor';
import { UserIdleModule } from 'angular-user-idle';

import { ResetPasswordComponent } from './module/user-module/reset-password/reset-password.component';
import { HeaderModule } from './shared/components/header/header.module';
import { SideMenuModule } from './shared/components/side-menu/side-menu.module';

@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent,
    // HeaderComponent,
    // SideMenuCompon÷ent,
    SideMenuMobileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    ToastrModule.forRoot(),
    UserIdleModule.forRoot({idle: 120, timeout: 300, ping: 120}),
    HeaderModule,
    SideMenuModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
