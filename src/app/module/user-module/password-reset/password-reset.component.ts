import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MustMatch } from './comparePassword/must-match';
import { UserModuleService } from 'src/app/shared/services/user-module/user-module.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  myForm: FormGroup;
  public formSubmitted: Boolean = false;
  public userProfile :any;

  constructor(
    public activeModal: NgbActiveModal,
    public userModuleService: UserModuleService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder) { }

  private createForm() {
    this.myForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    }, {
        validator: MustMatch('password', 'confirmPassword')
      });
  }
  get formData() { 
    return this.myForm.controls; 
  }

  public getProfile(){
    this.userModuleService.getUserSession().subscribe((res) => {
      this.userProfile = res;
    });
  }

  public submitForm() {
    this.formSubmitted = true;
    if (this.myForm.valid) {
      let param={};
      param=this.userProfile;
      param['password'] =this.myForm.value.password;
      this.userModuleService.resetpassword(param).subscribe((res)=>{
        console.log(res);
        this.formSubmitted = false;
        this.toastr.success('Password Updated Successfully');
        this.activeModal.close(this.myForm.value);
      }, (error)=>{
        this.formSubmitted = false;
      });
    }
  }

  ngOnInit() {
    this.createForm();
    this.getProfile();
  }

}
