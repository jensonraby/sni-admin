import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FloatButtonComponent } from './float-button/float-button.component';
import { PaymentComponent } from './payment/payment.component';
import { CopyRightComponent } from './copy-right/copy-right.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { NgxLoadingModule } from 'ngx-loading';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { PricingListComponent } from './pricing-list/pricing-list.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { UnsubscribeComponent } from './unsubscribe/unsubscribe.component';
import { SniInputModule } from 'src/app/shared/elements/sni-input/sni-input.module';

@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    ForgotPasswordComponent,
    FloatButtonComponent,
    PaymentComponent,
    CopyRightComponent,
    ResetPasswordComponent,
    PrivacyPolicyComponent,
    PricingListComponent,
    ContactUsComponent,
    UnsubscribeComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    NgbModalModule.forRoot()
  ],
  entryComponents: [UnsubscribeComponent]
})
export class UserModule { }
