import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, 
    private loginService: LoginService, 
    private router: Router,
    private toastr: ToastrService) { }

  public loginForm: FormGroup;
  public formSubmitted = false;

  private createForm() {
    this.loginForm = this.formBuilder.group({
      usernameOrEmail: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }

  failedAttempt(param){
    console.log(4564)
    this.loginService.failattempt(param).subscribe((res)=>{
     console.log(res)
    }, (err) => {
      // this.failedAttempt(this.loginForm.value);
      this.formSubmitted = false;
      this.toastr.error(err.error.message);
     
    })
  }

  submitForm() {
    if (this.loginForm.get('usernameOrEmail') && this.loginForm.get('usernameOrEmail').value && this.loginForm.get('password') && this.loginForm.get('password').valid) {
      this.formSubmitted = true;
      this.loginService.authLogin(this.loginForm.value).subscribe(response => {
        if(response.message =='Success'){
          this.formSubmitted = false;
          localStorage.setItem('sniUserData', JSON.stringify(this.loginForm.value));
          localStorage.setItem('sniUser', response.accessToken);
          this.router.navigate(['/dashboard']);
        }else{
          console.log(4564)
          this.failedAttempt(this.loginForm.value);
          this.formSubmitted = false;
        }
       
      }, (err) => {
        if(err.status==400){
          this.toastr.error(err.error.message);
        }else{
          this.failedAttempt(this.loginForm.value);
        }
        this.formSubmitted = false;
      });
    } else {
      this.formSubmitted = false;
    }
  }

  ngOnInit() {
    this.createForm();
  }

}
