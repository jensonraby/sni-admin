import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private router: Router,
    private toastr: ToastrService) { }

  // public verificationCode: boolean = false;
 
  public Code: boolean = false;
  public resetForm: FormGroup;
  public verifyForm: FormGroup;
  public formSubmitted = false;
  public submitError: boolean = false;

  private createForm() {
    this.resetForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]]
    });
  }

  private createVerifyForm() {
    this.verifyForm = this.formBuilder.group({
      verification: [null, [Validators.required]]
    });
  }

  submitForm() {
    if (this.resetForm.valid) {
      this.formSubmitted = true;
      this.loginService.authReset(this.resetForm.value).subscribe(response => {
        this.formSubmitted = false;
        this.toastr.success(response['message']);
        // this.verificationCode = true;
        this.router.navigate(['user/login']);
      }, (err) => {
        this.formSubmitted = false;
        this.toastr.error(err.error.message);
      });
    } else {
      this.submitError = true;
    }
  }

  verifyCode() {
    console.log(this.verifyForm.value);
  }


  ngOnInit() {
    this.createForm();
    this.createVerifyForm();
  }

}
