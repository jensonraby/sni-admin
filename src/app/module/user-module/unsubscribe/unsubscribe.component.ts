import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss']
})
export class UnsubscribeComponent implements OnInit {

  public form: any;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public activeModal: NgbActiveModal
    ) { }

  submitUnsubscribe() {
    if (this.form.valid) {
      console.log(this.form.value.eamil);
      this.toastr.success("You have been unsubscribed and will no longer hear from us.");
      this.activeModal.close('Modal Closed');

    }
  }

  private createForm() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]],
    });
  }

  ngOnInit() {
    this.createForm();
  }

}
