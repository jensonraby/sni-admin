import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { LoginService } from 'src/app/shared/services/login/login.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private router: Router,
    private toastr: ToastrService) { }

  public signupForm: FormGroup;
  public formSubmitted = false;
  public submitError: boolean = false;

  private createForm() {
    this.signupForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]],
      mobile: [null, [Validators.required]],
      password: [null, [Validators.required]],
      name: [null, [Validators.required]]
    });
  }

  submitForm() {
    if (this.signupForm.valid) {
      this.formSubmitted = true;
      this.loginService.authSignup(this.signupForm.value).subscribe(response => {
        this.formSubmitted = false;
        this.router.navigate(['user/login']);
        this.toastr.success(response.message);
      }, (err) => {
        console.log(err);
        this.formSubmitted = false;
        if(err.error.success ==false){
          this.toastr.error(err.error.message);
          return
        }
        if(err.error)
        this.toastr.error(err.error.errors[0].field + ' ' + err.error.errors[0].defaultMessage);
      });
    } else {
      this.formSubmitted = false;
    }
  }

  ngOnInit() {
    this.createForm();
  }

}
