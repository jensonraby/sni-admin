import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PaymentComponent } from './payment/payment.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { PricingListComponent } from './pricing-list/pricing-list.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: '/login'
  // },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'signup', component: SignupComponent
  },
  {
    path: 'forgot-password', component: ForgotPasswordComponent
  },
  {
    path: 'pricing', component: PricingListComponent
  },
  {
    path: 'payment', component: PaymentComponent
  },
  {
    path: 'reset-password/:token', component: ResetPasswordComponent
  },
  {
    path: 'privacy', component: PrivacyPolicyComponent
  },
  {
    path: 'contact-us', component: ContactUsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
