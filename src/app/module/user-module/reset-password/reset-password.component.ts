import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService) { }

  public passwordForm: FormGroup;
  public formSubmitted = false;
  public submitError: boolean = false;
  public token: any;

  private createPasswordForm() {
    this.passwordForm = this.formBuilder.group({
      password: [null, [Validators.required]]
    });
  }

  verifyPassword() {
    if (this.passwordForm.valid) {
      this.formSubmitted = true;
      let param = {};
      param['resetToken'] = this.token;
      param['password'] = this.passwordForm.value.password;
      this.loginService.resetPassword(param).subscribe(response => {
        this.toastr.success(response['message']);
        this.router.navigate(['user/login']);
      }, (err) => {
        console.log(err);
        this.toastr.error(err.error.message);
      });
    } else {
      this.submitError = true;
    }
    console.log(this.passwordForm.value);
  }

  verifyToken() {
    this.loginService.checkToken(this.token).subscribe(response => {
      console.log(response)
    }, (err) => {
      this.toastr.error(err.error.message);
    });
  }

  ngOnInit() {
    this.createPasswordForm();
    this.route.params.subscribe(params => {
      this.token = params.token;
      this.verifyToken();
    });
  }

}
