import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { LandingComponent } from './landing/landing.component';
import { UserListComponent } from './user-list/user-list.component';
import { AgGridModule } from 'ag-grid-angular';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { ActionComponent } from './user-list/action/action.component';
import { UserDetailsComponent } from './user-list/user-details/user-details.component';
import { NgxLoadingModule } from 'ngx-loading';
import { AssignRoleComponent } from './assign-role/assign-role.component';
// import { PasswordResetComponent } from '../user-module/contact-us/password-reset/password-reset.component';
import { ImageUploadModule } from 'src/app/shared/components/image-upload/image-upload.module';

@NgModule({
  declarations: [
    LandingComponent,
    UserListComponent,
    ProfileDetailComponent,
    ActionComponent,
    UserDetailsComponent,
    AssignRoleComponent,
    // PasswordResetComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    AgGridModule.withComponents([
      ActionComponent
    ]),
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    ImageUploadModule
  ]
})
export class DashboardModule { }
