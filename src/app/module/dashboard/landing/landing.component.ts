import { DashboardService } from './../../../shared/services/dashboard/dashboard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(private dashboardService: DashboardService) { }


  ngOnInit() {
   
  }

}
