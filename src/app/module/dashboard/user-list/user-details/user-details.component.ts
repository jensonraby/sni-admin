import { DashboardService } from './../../../../shared/services/dashboard/dashboard.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  private id: any;
  public userProfile: any;
  constructor(private route: ActivatedRoute,
    private dashboardService: DashboardService) { }

  getProfile(id) {
    this.dashboardService.getProfile(id).subscribe((res) => {
      console.log(res);
      this.userProfile = res;
    })
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params)
      this.id = params['id'];
      this.getProfile(this.id);
    });
    // console.log(this.id);
  }

}
