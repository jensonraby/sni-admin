import { DashboardService } from './../../../../shared/services/dashboard/dashboard.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements ICellRendererAngularComp {
  public params: any;
  public formSubmitted = false;

  constructor(private router: Router,
    private dashboardService: DashboardService,
    private toastr: ToastrService) {

  }

  agInit(params: any): void {
    this.params = params;
    this.refresh(params);
  }

  public invokeParentMethod() {
    this.formSubmitted = true;
    console.log(this.params);
    if (this.params.data.active == false) {
      this.params.data.active = !this.params.data.active;
      this.dashboardService.activateProfile(this.params.value).subscribe((response) => {
        console.log(response);
        this.params.setValue(this.params.value);
        this.toastr.success(response.message);
        this.formSubmitted = false;
      })
    } else {
      this.params.data.active = !this.params.data.active;
      this.dashboardService.deactivateProfile(this.params.value).subscribe((response) => {
        console.log(response);
        this.params.setValue(this.params.value);
        this.toastr.success(response.message);
        this.formSubmitted = false;
      })
    }


  }

  public ViewProfile() {
    this.router.navigate(['/dashboard/list', this.params.value]);
  }
  public assignRole() {
    this.router.navigate(['/dashboard/assign-role', this.params.value]);
  }


  refresh(params): boolean {
    return params;
  }
}