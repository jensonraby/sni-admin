import { DashboardService } from './../../../shared/services/dashboard/dashboard.service';
import { ActionComponent } from './action/action.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ColumnGrid } from 'src/app/shared/models/grid-column.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  private gridApi: any;
  public gridOptions: any;
  private columnDefs: any;
  private defaultColDef: any;
  public rowData: any;
  public listUser: [];
  public searchForm: FormGroup;

  public cellClass = function () {
    return { 'line-height': '40px' };
  }

  constructor(private formBuilder: FormBuilder,private dashboardService: DashboardService) {
    this.columnDefs = [
      new ColumnGrid('NAME', 'name', true, false, true, false, 30, "NAME", this.cellClass),
      new ColumnGrid('USERNAME', 'username', true, false, true, false, 50, "USERNAME", this.cellClass),
      new ColumnGrid('EMAIL', 'email', true, false, true, false, 80, "EMAIL", this.cellClass),
      new ColumnGrid('MOBILE', 'mobile', true, false, true, false, 40, "MOBILE", this.cellClass),
      new ColumnGrid('ROLE', '', true, false, true, false, 40, "ROLE", this.cellClass, false, 'data.roles[0].name'),
      new ColumnGrid('Action', 'id', true, false, true, false, 100, "Action", this.cellClass, ActionComponent),
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true
    };

    this.gridOptions = {
      columnDefs: this.columnDefs,
      defaultColDef: this.defaultColDef,
      pagination: true,
      rowHeight: 45,
      paginationPageSize: 10,
      headerHeight: 35
    };
  }

  // dataSource = {
  //   rowCount: null,
  //   getRows: (params) => {
  //     this.dashboardService.getUserList().subscribe((res) => {
  //       this.rowData = res.data;
  //       params.successCallback(this.rowData, this.rowData.length);
  //     });
  //   }
  // }

  searchData(param: any){
    this.gridOptions.api.setQuickFilter(param);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();;
    // this.gridApi.setDatasource(this.dataSource);
    this.dashboardService.getUserList().subscribe((res) => {
      this.rowData = res;
    });

  }

  getUserList() {
    this.dashboardService.getUserList().subscribe((res) => {
      this.rowData = res;
    })
  }

  private createForm() {
    this.searchForm = this.formBuilder.group({
      search: [null, []],
    });
  }

  ngOnInit() {
    this.createForm()
  }
  
}
