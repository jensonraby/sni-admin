import { UserDetailsComponent } from './user-list/user-details/user-details.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { UserListComponent } from './user-list/user-list.component';
import { AssignRoleComponent } from './assign-role/assign-role.component';
// import { PasswordResetComponent } from './password-reset/password-reset.component';

const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'list', component: UserListComponent },
  { path: 'list/:id', component: UserDetailsComponent },
  { path: 'assign-role/:id', component: AssignRoleComponent },
  { path: 'profile', component: ProfileDetailComponent },
  // { path: 'password-reset', component: PasswordResetComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
