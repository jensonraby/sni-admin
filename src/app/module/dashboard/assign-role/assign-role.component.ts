import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/shared/services/dashboard/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-assign-role',
  templateUrl: './assign-role.component.html',
  styleUrls: ['./assign-role.component.scss']
})
export class AssignRoleComponent implements OnInit {

  private id: any;
  public userProfile: any;
  public selectedRole: any;
  public listRole: any;

  constructor(private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService) { }

  getProfile(id) {
    this.dashboardService.getProfile(id).subscribe((res) => {
      this.userProfile = res;
      this.selectedRole = res.roles[0].id;
    })
  }

  search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].id == nameKey) {
        return myArray[i];
      }
    }
  }

  getRole() {
    this.dashboardService.getRole().subscribe((res) => {
      this.listRole = res;
      this.getProfile(this.id);
    })
  }

  updateUserRole() {
    var resultObject = this.search(this.selectedRole, this.listRole);
    let param = {
      id: this.id,
      roles: [
        {
          "id": resultObject.id,
          "name": resultObject.name,
          "permissions": []
        }
      ],
    }
    this.dashboardService.updateRole(param).subscribe((res) => {
      this.toastr.success(res.message);
      this.navidateToList();
    })
  }

  navidateToList() {
    this.router.navigate(['/dashboard/list']);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getRole();
    });
  }
}
