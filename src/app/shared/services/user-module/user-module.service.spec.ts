import { TestBed } from '@angular/core/testing';

import { UserModuleService } from './user-module.service';

describe('UserModuleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserModuleService = TestBed.get(UserModuleService);
    expect(service).toBeTruthy();
  });
});
