import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserModuleService {

  constructor(private http: HttpClient) { }

  getUserSession(): Observable<any> {
    let url = `/api/user/getUserProfile`;
    let credentials = JSON.parse(localStorage.getItem('sniUserData'));
    return this.http.post(url, credentials).pipe(catchError(error => throwError(error)))
  }

  userLogout(): Observable<any> {
    let url = `/api/users/logout`;
    // let credentials = JSON.parse(localStorage.getItem('sniUserData'));
    let credentials={};
    return this.http.post(url, credentials).pipe(catchError(error => throwError(error)))
  }

  resetpassword(params): Observable<any> {
    let url = `/api/auth/resetpassword`;
    // let credentials = JSON.parse(localStorage.getItem('sniUserData'));
    return this.http.post(url, params).pipe(catchError(error => throwError(error)))
  }
}
