import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private authLoginUrl: any;

  constructor(private http: HttpClient) { }

  authLogin(credentials: any): Observable<any> {
    this.authLoginUrl = `/api/auth/signin`;
    return this.http.post(this.authLoginUrl, credentials).pipe(catchError(error => throwError(error)))
  }

  authSignup(params: any): Observable<any> {
    let authSignupUrl = `/api/auth/signup`;
    return this.http.post(authSignupUrl, params).pipe(catchError(error => throwError(error)))
  }

  authReset(params: any) {
    let authSignupUrl = `/api/auth/forgotpassword?email=${params.email}`;
    // return of({ success: 'check your inbox' })
    return this.http.post(authSignupUrl, {}).pipe(catchError(error => throwError(error)))
  }

  resetPassword(params: any) {
    let url = `/api/auth/resetpassword`;
    return this.http.post(url, params).pipe(catchError(error => throwError(error)))
  }

  checkToken(params: any) {
    let url = `/api/auth/validateresetpasswordtoken?token=${params}`;
    return this.http.get(url).pipe(catchError(error => throwError(error)))
  }

  failattempt(params: any) {
    let url = `/api/auth/failattempt`;
    return this.http.post(url, params).pipe(catchError(error => throwError(error)))
  }
}