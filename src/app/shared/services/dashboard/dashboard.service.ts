import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import * as countryJson from  './country.json'

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) {}

  getUserList(): Observable<any> {
    let url = `/api/user/getAllUsers`;
    return this.http.get(url).pipe(catchError(error => throwError(error)))
  }

  updateProfile(params: any): Observable<any> {
    let url = `/api/user/updateuserprofile`;
    return this.http.post(url, params).pipe(catchError(error => throwError(error)))
  }

  getProfile(params: any): Observable<any> {
    let url = `/api/user/getUserProfileByID?id=${params}`;
    return this.http.post(url, {}).pipe(catchError(error => throwError(error)))
  }

  activateProfile(params: any): Observable<any> {
    let url = `/api/user/activate?id=${params}`;
    return this.http.get(url).pipe(catchError(error => throwError(error)))
  }

  deactivateProfile(params: any): Observable<any> {
    let url = `/api/user/deactivate?id=${params}`;
    return this.http.get(url).pipe(catchError(error => throwError(error)))
  }

  getRole(): Observable<any> {
    let url = `/api/user/getAllRoles`;
    return this.http.get(url).pipe(catchError(error => throwError(error)))
  }

  updateRole(param): Observable<any> {
    let url = `/api/user/updateuserrole`;
    return this.http.post(url, param).pipe(catchError(error => throwError(error)))
  }

  updateImage(param): Observable<any> {
    let url = `/api/image/uploadFile`;
    return this.http.post(url, param).pipe(catchError(error => throwError(error)))
  }

  allCountries(){
  return countryJson;
  }
}
