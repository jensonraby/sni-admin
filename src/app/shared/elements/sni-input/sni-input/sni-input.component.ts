import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-sni-input',
  templateUrl: './sni-input.component.html',
  styleUrls: ['./sni-input.component.scss']
})
export class SniInputComponent implements OnInit {
  @Input() inputForm: FormGroup;
  @Input() formControl: any;
  @Input() inputName: any;

  constructor() { }

  ngOnInit() {
  }

}
