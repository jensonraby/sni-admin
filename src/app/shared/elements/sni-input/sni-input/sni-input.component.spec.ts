import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SniInputComponent } from './sni-input.component';

describe('SniInputComponent', () => {
  let component: SniInputComponent;
  let fixture: ComponentFixture<SniInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SniInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SniInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
