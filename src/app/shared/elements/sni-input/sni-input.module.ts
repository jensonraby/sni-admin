import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SniInputComponent } from './sni-input/sni-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SniInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    SniInputComponent
  ]
})
export class SniInputModule { }
