export class ColumnGrid {
    constructor(
        public headerName: String,
        public field: String,
        public sortable?: Boolean,
        public filter?: Boolean,
        public resizable?: Boolean,
        public editable?: Boolean,
        public width?: Number,
        public tooltipField?:String,
        public cellStyle?:any,
        public cellRendererFramework?:any,
        public valueGetter?:any,
        public cellRenderer?:any,
        public headerCheckboxSelection?:Boolean,
        public headerCheckboxSelectionFilteredOnly?:Boolean,
        public checkboxSelection?:Boolean,
        public cellEditor?:any,
        public colId?:any,
        public sort?:any,   
       
    ) { }
}