import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit {

  addActive :boolean =true;
  constructor() { }

  activateClass(){
    this.addActive = !this.addActive;    
  }

  ngOnInit() {
  }

}
