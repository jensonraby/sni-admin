
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable()
export class ImageUploadService {

  constructor(private http: HttpClient){}


  public uploadImage(image: File): Observable<string | any> {
    const formData = new FormData();

    formData.append('file', image);
    formData.append('userid', '3');
    let data={
      userid :3,
      file :image
    }

    return this.http.post('https://tranquil-temple-16670.herokuapp.com/api/image/uploadFile', data).pipe(map(((json: any) =>  json.imageUrl)));
  }
}
