import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { SniInputModule } from '../../elements/sni-input/sni-input.module';
import { HeaderOptionsModule } from '../header-options/header-options.module';

@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    SniInputModule,
    HeaderOptionsModule
  ],
  exports:[
    HeaderComponent
  ]
})
export class HeaderModule { }
