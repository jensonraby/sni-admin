import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideMenuMobileComponent } from './side-menu-mobile.component';

describe('SideMenuMobileComponent', () => {
  let component: SideMenuMobileComponent;
  let fixture: ComponentFixture<SideMenuMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideMenuMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideMenuMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
