import { UserModuleService } from './../../services/user-module/user-module.service';
import { Component, OnInit } from '@angular/core';
import * as item from '../side-menu/side-menu-items';

@Component({
  selector: 'app-side-menu-mobile',
  templateUrl: './side-menu-mobile.component.html',
  styleUrls: ['./side-menu-mobile.component.scss']
})
export class SideMenuMobileComponent implements OnInit {
  public userProfile: any;

  constructor(private userModuleService: UserModuleService) { }

  public menuItemList: any = item.menuItem;
  public hamburgerClass: boolean = false;

  activateClass(event: any) {
    // console.log(event)
    // var target = event.target || event.srcElement || event.currentTarget;
    // var idAttr = target.attributes.id;
    // console.log(idAttr);
    if (event.subMenu) {
      event.active = !event.active;
    }
  }

  getUser(){
    this.userModuleService.getUserSession().subscribe((res) =>{
      this.userProfile = res;
      this.userProfile.role = (this.userProfile.roles[0].name == 'ROLE_ADMIN') ? false : false;
    });
  }

  hamburgerClick() {
    this.hamburgerClass = !this.hamburgerClass;
  }

  ngOnInit() {
    this.getUser();
  }

}
