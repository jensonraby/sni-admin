import { UserModuleService } from './../../services/user-module/user-module.service';
import { Component, OnInit, Input } from '@angular/core';
import * as item from './side-menu-items';
import * as $ from 'jquery';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  public menuItemList: any = item.menuItem;
  public userProfile: any;

  constructor(private userModuleService: UserModuleService) { }

  activateClass(event: any) {
    // console.log(event)
    // var target = event.target || event.srcElement || event.currentTarget;
    // var idAttr = target.attributes.id;
    // console.log(idAttr);
    // if (event.subMenu) {
    //   event.active = !event.active;
    // }
  }

  getUser(){
    this.userModuleService.getUserSession().subscribe((res) =>{
      this.userProfile = res;
      this.userProfile.role = (this.userProfile.roles[0].name == 'ROLE_ADMIN') ? false : false;
    });
  }

  ngOnInit() {
    this.getUser();
    $('.menu-activated-on-click').on('click', 'li.has-sub-menu > a', function (event) {
      var $elem = $(this).closest('li');
      if ($elem.hasClass('active')) {
        $elem.removeClass('active');
      } else {
        $elem.closest('ul').find('li.active').removeClass('active');
        $elem.addClass('active');
      }
      return false;
    });
  }

}
