import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderOptionsModule } from '../header-options/header-options.module';
import { SideMenuComponent } from './side-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    SideMenuComponent
  ],
  imports: [
    CommonModule,
    HeaderOptionsModule,
    RouterModule
  ],
  exports:[
    SideMenuComponent
  ]
})
export class SideMenuModule { }
