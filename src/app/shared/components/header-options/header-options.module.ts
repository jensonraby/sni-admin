import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderOptionsComponent } from './header-options.component';
import { PasswordResetComponent } from 'src/app/module/user-module/password-reset/password-reset.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  declarations: [
    HeaderOptionsComponent,
    PasswordResetComponent
  ],
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({})
  ],
  exports:[
    HeaderOptionsComponent
  ],
  entryComponents:[
    PasswordResetComponent
  ]
})
export class HeaderOptionsModule { }
