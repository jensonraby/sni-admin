import { Component, OnInit, Input } from '@angular/core';
import { UserModuleService } from './../../services/user-module/user-module.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PasswordResetComponent } from 'src/app/module/user-module/password-reset/password-reset.component';

@Component({
  selector: 'app-header-options',
  templateUrl: './header-options.component.html',
  styleUrls: ['./header-options.component.scss']
})
export class HeaderOptionsComponent implements OnInit {

  public userProfile: any;
  @Input()sideHeader:any;

  constructor(private userModuleService: UserModuleService,
    private modalService: NgbModal,
    private router: Router) { }

  userLogoutSession(){
    localStorage.removeItem('sniUser');
    localStorage.removeItem('sniUserData');
    this.router.navigate(['user/login']);
    // this.userModuleService.userLogout().subscribe((res)=>{
    //   console.log(res);

    // })
  }

  profileDetails(){
    this.router.navigate(['/dashboard/profile']);
  }

  getUser(){
    this.userModuleService.getUserSession().subscribe((res) =>{
      this.userProfile = res;
      this.userProfile.role = (this.userProfile.roles[0].name == 'ROLE_ADMIN') ? false : false;
    });
  }

  resetPassword() {
      const modalRef = this.modalService.open(PasswordResetComponent);
      modalRef.result.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.getUser();
  }

}
